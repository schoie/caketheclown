﻿using UnityEngine;
using System.Collections;

public class ResetClown : MonoBehaviour {

	void ResetClownOnHit () 
	{
		gameObject.transform.localScale = Vector3.one;
		gameObject.GetComponentInChildren<CollisionBehaviour> ().collided = false;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
